Сервис клиент к конфигурационному серверу
+ он клиент к лругим сервисам, которые зареганы на еврике.



## 00001.
RestTemplate can be automatically configured to use ribbon. To create a load balanced RestTemplate create a RestTemplate @Bean and use the @LoadBalanced qualifier.

WARNING
A RestTemplate bean is no longer created via auto configuration. It must be created by individual applications.
@Configuration
public class MyConfiguration {

    @LoadBalanced
    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }
}

public class MyClass {
    @Autowired
    private RestTemplate restTemplate;

    public String doOtherStuff() {
        String results = restTemplate.getForObject("http://stores/stores", String.class);
        return results;
    }
}




## 00002.

Ignore Network Interfaces
Sometimes it is useful to ignore certain named network interfaces so they can be excluded from Service Discovery registration
 (eg. running in a Docker container). A list of regular expressions can be set that will cause the desired network interfaces to be ignored.
  The following configuration will ignore the "docker0" interface and all interfaces that start with "veth".

application.yml
spring:
  cloud:
    inetutils:
      ignoredInterfaces:
        - docker0
        - veth.*






## 00003.Config Client Fail Fast
         In some cases, it may be desirable to fail startup of a service if it cannot connect to the Config Server. If this is the desired behavior,
         set the bootstrap configuration property spring.cloud.config.failFast=true and the client will halt with an Exception.




## 00004.
Locating Remote Configuration Resources
The Config Service serves property sources from /{name}/{profile}/{label}, where the default bindings in the client app are

"name" = ${spring.application.name}

"profile" = ${spring.profiles.active} (actually Environment.getActiveProfiles())

"label" = "master"

All of them can be overridden by setting spring.cloud.config.* (where * is "name", "profile" or "label").
 The "label" is useful for rolling back to previous versions of configuration; with the
 default Config Server implementation it can be a git label, branch name or commit id.
 Label can also be provided as a comma-separated list, in which case the items in the list are tried on-by-one until one succeeds.
  This can be useful when working on a feature branch, for instance, when you might want to align the config label with your branch,
  but make it optional (e.g. spring.cloud.config.label=myfeature,develop).


## 00001.
## 00001.
## 00001.
## 00001.
## 00001.
## 00001.
## 00001.
## 00001.
## 00001.
## 00001.
## 00001.
## 00001.
## 00001.
## 00001.
## 00001.
## 00001.
## 00001.
## 00001.
## 00001.
