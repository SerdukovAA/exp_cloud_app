chmod +x gradlew
sh gradlew clean build

cp -rf build/libs/user_service-0.0.1-SNAPSHOT.jar docker/app.jar

docker info


docker stop userservice1 || echo "userservice1 is already stopped"
docker stop userservice2 || echo "userservice1 is already stopped"


docker rm userservice1 || echo "userservice1 is already removed"
docker rm userservice2 || echo "userservice2 is already removed"


docker build --no-cache=true -t exp_cloud_app/user_service docker/.

#версия должна быть выше 1.10 чтобы был доступен --ip

docker run -p 9011:9011 --net=cloud  -e "SPRING_PROFILE=node1" --hostname=userservice1 -d --name userservice1 --add-host="eureka2:172.21.1.1" --add-host="eureka2:172.21.1.2" --add-host="eureka3:172.21.1.3" exp_cloud_app/user_service
docker run -p 9012:9011 --net=cloud  -e "SPRING_PROFILE=node2" --hostname=userservice2 -d --name userservice2 --add-host="eureka2:172.21.1.1" --add-host="eureka2:172.21.1.2" --add-host="eureka3:172.21.1.3" exp_cloud_app/user_service

