docker info
docker stop nginx_server || echo "nginx_server is already stopped"
docker rm nginx_server || echo "nginx_server is already removed"
docker build --no-cache=true -t exp_cloud_app/nginx_server .
docker run -v '/opt/web_client:/usr/share/nginx/html:ro' --net=cloud --hostname=nginx -p 80:80  -d --add-host="zuul1:172.21.0.1" --add-host="zuul2:172.21.0.2" -d --name nginx_server exp_cloud_app/nginx_server