chmod +x gradlew
sh gradlew clean build

cp -rf build/libs/config-server-0.0.1-SNAPSHOT.jar docker/app.jar

docker info

docker stop config1 || echo "config1 is already stopped"
docker stop config2 || echo "config2 is already stopped"


docker rm config1 || echo "config1 is already removed"
docker rm config2 || echo "config2 is already removed"


docker build --no-cache=true -t exp_cloud_app/config_service docker/.

#версия должна быть выше 1.10 чтобы был доступен --ip

docker run -p 8881:8888 -v '/opt/git_spring_config:/opt/git_spring_config:ro' --net=cloud  --hostname=config1 -d --name config1 --add-host="eureka2:172.21.1.1" --add-host="eureka2:172.21.1.2" --add-host="eureka3:172.21.1.3" exp_cloud_app/config_service
docker run -p 8882:8888 -v '/opt/git_spring_config:/opt/git_spring_config:ro' --net=cloud  --hostname=config2 -d --name config2 --add-host="eureka2:172.21.1.1" --add-host="eureka2:172.21.1.2" --add-host="eureka3:172.21.1.3" exp_cloud_app/config_service

