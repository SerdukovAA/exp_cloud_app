посомтреть проперти - http://localhost:8889/config-consumer/default



Получается так, что приложения которые знают что есть конигурационный сервер знают как с ним взаимодействовать.
По факту конфигурационный сервер, самодостаточен по дефолту

## 0001. Чтобы приложения могли переназначать проперти локально, можно на конфиг сервер грантовать приложения, типа:
spring.cloud.config.allowOverride=true
Для более тонкой настройки:
spring.cloud.config.overrideNone=true
spring.cloud.config.overrideSystemProperties=false


## 0002. Приложения слушают EnvironmentChangedEvent. And react to the change in a couple of standard ways
(additional ApplicationListeners can be added as @Beans by the user in the normal way).


## 0003. Re-binding поможет если нужно изменить poolSize например. Но для атамрных изнеменений нескольких параметром применяют фичу Refresh Scope


## 0004. The Config Server needs to know which repository to manage. There are several choices here, but we’ll use a Git-based filesystem repository.
You could as easily point the Config Server to a Github or GitLab repository, as well. On the file system, create a new directory and git init it.
Then add a file called a-bootiful-client.properties to the Git repository. Make sure to also git commit it, as well.
Later, you will connect to the Config Server with a Spring Boot application whose spring.application.name property identifies it as a-bootiful-client to the Config Server.
This is how the Config Server will know which set of configuration to send to a specific client. It will also send all the values from any file
 named application.properties or application.yml in the Git repository, though conflicting property keys in the more specifically named file
 (a-bootiful-client.properties) override those in application.properties or application.yml.



## 0005. У клиента если мы не используем еврику ставится типа:
spring.application.name=a-bootiful-client
spring.cloud.config.uri=http://localhost:8888

если через еврику:

spring:
  application:
    name: config-consumer
  cloud:
    config:
      enabled: true
      discovery:
        enabled: true
        serviceId: my-config-server


## 0006. По-умолчанию клиент считывает значения с конфигурационного сервера при статре, и больше нет.
 Но можно пометить беан:


@RefreshScope
@RestController
class MessageRestController {

    @Value("${message}")
    private String message;

    @RequestMapping("/message")
    String getMessage() {
        return this.message;
    }
}

 Change the message key in the a-bootiful-client.properties file in the Git repository to something different (Hello Spring!, perhaps?).
 You can confirm that the Config Server sees the change by visiting http://localhost:8888/a-bootiful-client/default.
 You need to invoke the refresh Spring Boot Actuator endpoint in order to force the client to refresh itself and draw the new value in.
 Spring 's Actuator exposes operational endpoints, like health checks and environment information, about an application.
 In order to use it you must add org.springframework.boot:spring-boot-starter-actuator to the configuration-client’s CLASSPATH.
 You can invoke the `refresh Actuator endpoint by sending an empty HTTP POST to the client’s refresh endpoint, http://localhost:8080/refresh,
 and then confirm it worked by reviewing the http://localhost:8080/message endpoint.




## 0007. Кастомная реакция на изменения конфигурации:
public class YourEventListener implements ApplicationListener<EnvironmentChangeEvent> {
    @Override
    public void onApplicationEvent(EnvironmentChangeEvent event) {
        // do stuff
    }
}



## 0008.
The default strategy for locating property sources is to clone a git repository (at spring.cloud.config.server.git.uri)
and use it to initialize a mini SpringApplication. The mini-application’s Environment is used to enumerate property
sources and publish them via a JSON endpoint.



## 0009.

Как сервер конфигурации используется на клиенте?
Нужно запустить клиент как Spring Boot приложение которое зависит от spring-cloud-config-client
По стандарту добавляем зависимость via a Spring Boot starter org.springframework.cloud:spring-cloud-starter-config

Я почему то использовал - compile('org.springframework.cloud:spring-cloud-config-client')

Потом если не используем еврику:
spring.application.name=a-bootiful-client
spring.cloud.config.uri: http://myconfigserver.com
Если используем писал Выше.




## 00010.
Чтобы сервер заработал


@SpringBootApplication
@EnableConfigServer
public class ConfigServer {
  public static void main(String[] args) {
    SpringApplication.run(ConfigServer.class, args);
  }
}


+ укажи порт и наименование сервер конфигурации если он будет использоваться в еврике

Можно указывать локальные репозитории:
spring.cloud.config.server.git.uri: file://${user.home}/config-repo



## 00011.

The Environment resources are parametrized by three variables:

{application} maps to "spring.application.name" on the client side;
{profile} maps to "spring.profiles.active" on the client (comma separated list); and
{label} which is a server side feature labelling a "versioned" set of config files.

## 00012.

Placeholders in Git URI
Spring Cloud Config Server supports a git repository URL with placeholders for the {application} and {profile} (and {label} if you need it, but remember that the label is applied as a git label anyway). So you can easily support a "one repo per application" policy using (for example):

spring:
  cloud:
    config:
      server:
        git:
          uri: https://github.com/myorg/{application}
or a "one repo per profile" policy using a similar pattern but with {profile}.


## 00013. Индикатор здоровья
 ## индикатор здоровья
                health:
                    repositories:
                        myservice:
                            name: config-consumer
                            profiles: dev


Чтобы его чекнуть  - http://localhost:8889/health

Можно отключить если не нужен - spring.cloud.config.server.health.enabled=false.




## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.
## 0001.