package com.example.filters.pre;


import javax.servlet.http.HttpServletRequest;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.ZuulFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleFilter extends ZuulFilter {
    private static final String X_FORWARDED_FOR = "X-Forwarded-For";
    private static Logger log = LoggerFactory.getLogger(SimpleFilter.class);

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();

    // Rely on HttpServletRequest to retrieve the correct remote address from upstream X-Forwarded-For header
    HttpServletRequest request = ctx.getRequest();
    String remoteAddr = request.getRemoteAddr();

    // Pass remote address downstream by setting X-Forwarded for header again on Zuul request
    log.debug("Settings X-Forwarded-For to: {}", remoteAddr);
    System.out.println("Settings X-Forwarded-For to: " + remoteAddr);
    ctx.getZuulRequestHeaders().put(X_FORWARDED_FOR, remoteAddr);
        return null;


    }

}