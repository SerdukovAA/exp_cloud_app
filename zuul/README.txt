https://spring.io/guides/gs/routing-and-filtering/

To forward requests from the Gateway application, we need to tell Zuul the routes that it should watch and
the services to which to forward requests to those routes. We specify routes using properties
under zuul.routes. Each of our microservices can have an entry under zuul.routes.NAME, where
NAME is the application name (as stored in the spring.application.name property).

Add the application.properties file to a new directory, src/main/resources, in the Gateway application. It should look like this:

gateway/src/main/resources/application.properties

zuul.routes.book.path=/books/**
zuul.routes.book.url=http://localhost:8090

ribbon.eureka.enabled=false

server.port=8080