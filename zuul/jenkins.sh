chmod +x gradlew
sh gradlew clean build

cp -rf build/libs/zuul-1.0-SNAPSHOT.jar docker/

docker info


docker stop zuul1 || echo "zuul1 is already stopped"
docker stop zuul2 || echo "zuul2 is already stopped"


docker rm zuul1 || echo "zuul1 is already removed"
docker rm zuul2 || echo "zuul2 is already removed"


docker build --no-cache=true -t exp_cloud_app/zuul docker/.

#версия должна быть выше 1.10 чтобы был доступен --ip

docker run -p 8081:8081 --net=cloud -e "SPRING_PROFILE=zuul1" --hostname=zuul1 -d --name zuul1 --add-host="eureka2:172.21.1.1" --add-host="eureka2:172.21.1.2" --add-host="eureka3:172.21.1.3" exp_cloud_app/zuul
docker run -p 8082:8082 --net=cloud -e "SPRING_PROFILE=zuul2" --hostname=zuul2 -d --name zuul2 --add-host="eureka2:172.21.1.1" --add-host="eureka2:172.21.1.2" --add-host="eureka3:172.21.1.3" exp_cloud_app/zuul

