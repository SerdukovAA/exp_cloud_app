chmod +x gradlew
sh gradlew clean build

cp -rf build/libs/auth-server-0.0.1-SNAPSHOT.jar docker/app.jar

docker info

docker stop auth1 || echo "auth1 is already stopped"
docker stop auth2 || echo "auth2 is already stopped"

docker rm auth1 || echo "auth1 is already removed"
docker rm auth2 || echo "auth2 is already removed"



docker build --no-cache=true -t exp_cloud_app/auth_server docker/.

#версия должна быть выше 1.10 чтобы был доступен --ip

docker run -p 7777:7777 --net=cloud  --hostname=auth1 -d --name auth1 --add-host="redis_server:172.21.1.4" exp_cloud_app/auth_server
docker run -p 7778:7777 --net=cloud  --hostname=auth2 -d --name auth2 --add-host="redis_server:172.21.1.4" exp_cloud_app/auth_server

